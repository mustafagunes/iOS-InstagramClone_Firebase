//
//  singInVC.swift
//  InstagramClone(Firebase)
//
//  Created by Mustafa GUNES on 18.11.2017.
//  Copyright © 2017 Mustafa GUNES. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class singInVC: UIViewController {

    
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func singInClicked(_ sender: Any) {
        
        if emailText.text != "" && passwordText.text != "" {
            
            Auth.auth().signIn(withEmail: emailText.text!, password: passwordText.text!, completion: { (user, error) in
                
                if error != nil {
                    
                    let alert = UIAlertController(title: "Error !", message: error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert) // error?.localizedDescription => messageBox(ex) gibi yerel hata gösterici
                    let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
                    
                    alert.addAction(okButton)
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                    UserDefaults.standard.set(user!.email, forKey: "user")
                    UserDefaults.standard.synchronize()
                    
                    let delegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                    delegate.rememberLogin()
                }
            })
        }
            
        else
        {
            let alert = UIAlertController(title: "Error !", message: "Kullanıcı Adı ve Şifre Gerekli !", preferredStyle: UIAlertControllerStyle.alert)
            let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
            
            alert.addAction(okButton)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func singUpClicked(_ sender: Any) {
        
        if emailText.text != "" && passwordText.text != "" {
            
            Auth.auth().createUser(withEmail: emailText.text!, password: passwordText.text!, completion: { (user, error) in
                
                if error != nil {
                    
                    let alert = UIAlertController(title: "Error !", message: error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert) // error?.localizedDescription => messageBox(ex) gibi yerel hata gösterici
                    let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
                    
                    alert.addAction(okButton)
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                    UserDefaults.standard.set(user!.email, forKey: "user")
                    UserDefaults.standard.synchronize()
                    
                    let delegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
                    delegate.rememberLogin()
                }
            })
        }
        else
        {
            let alert = UIAlertController(title: "Error !", message: "Kullanıcı Adı ve Şifre Gerekli !", preferredStyle: UIAlertControllerStyle.alert)
            let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
            
            alert.addAction(okButton)
            self.present(alert, animated: true, completion: nil)
        }
    }
}
