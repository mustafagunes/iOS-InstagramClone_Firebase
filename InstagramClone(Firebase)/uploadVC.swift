//
//  SecondViewController.swift
//  InstagramClone(Firebase)
//
//  Created by Mustafa GUNES on 18.11.2017.
//  Copyright © 2017 Mustafa GUNES. All rights reserved.
//

import UIKit
import Firebase // eklendi
import FirebaseAuth // eklendi
import FirebaseStorage // eklendi
import FirebaseDatabase // eklendi

class uploadVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate { // eklendi.

    
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var postComment: UITextView!
    
    var uuid = NSUUID().uuidString
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        postImage.isUserInteractionEnabled = true
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(uploadVC.choosePhoto))
        postImage.addGestureRecognizer(recognizer)
    }
    
    @IBAction func postButtonClicked(_ sender: Any) {
        
        let mediaFolder = Storage.storage().reference().child("media") // media dosyasının yolu.
        
        if let data = UIImageJPEGRepresentation(postImage.image!, 0.5) { // eğer resmi dataya cevirebilirsem.
            
            mediaFolder.child("\(uuid).jpg").putData(data, metadata: nil, completion: { (metadata, error) in
                
                if error != nil {
                    
                    let alert = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                    let OkButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
                    
                    alert.addAction(OkButton)
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                    // Database'e veriyi ekleme
                    
                    let imagURL = metadata?.downloadURL()?.absoluteString
                    let post = ["image" : imagURL!,"postedby" : Auth.auth().currentUser!.email!, "uuid" : self.uuid, "posttext" : self.postComment.text] as [String : Any]
                    Database.database().reference().child("users").child((Auth.auth().currentUser?.uid)!).child("post").childByAutoId().setValue(post)
                    
                    self.postImage.image = UIImage(named: "select.png")
                    self.postComment.text = ""
                    self.tabBarController?.selectedIndex = 0
                }
            })
        }
    }
    
    @objc func choosePhoto() {
        
        let picker = UIImagePickerController()
        
        picker.delegate = self
        picker.sourceType = .photoLibrary
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        postImage.image = info[UIImagePickerControllerEditedImage] as? UIImage // UIImage ye cast ediliyor.
        self.dismiss(animated: true, completion: nil)
    }
}
